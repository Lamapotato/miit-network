package miit.study;

import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class Client implements Runnable {
	
	private Buffer buffer;
	final Random random = new Random();
	private Semaphore mutex1;
	private Semaphore mutex2;
	private String s;
	private long start = 0;
	private long elapsedTime = 0;
	private byte[] check = {1,1,1};
	private byte[] noise = {2,1,1};
	//
	private Scanner reader = new Scanner(System.in);
	private String type = "Client";
	private String result = "";
	private String oldType = "Server";
	
	Client(Buffer buffer, Semaphore mutex1,Semaphore mutex2){
		this.buffer = buffer;
		this.mutex1 = mutex1;
		this.mutex2 = mutex2;
	}

	public void run(){
		//s = generate();
		//s = "test";
		while(true){
			switch(type){
			case "Client":
				elapsedTime = System.nanoTime() - start;
				//System.out.println("Время приема пакета: " + elapsedTime);
				//if(new Random().nextInt(10)>5)
				s = reader.nextLine();
				if(buffer.bytes.size() < buffer.size){
					boolean checked;
					if(new Random().nextInt(10)>9){
						checked = buffer.addWithCheck(addCheckBytes(s.getBytes()),check);	
					} else {
						checked = buffer.addWithCheck(addCheckBytes(s.getBytes()),noise);
					}
					while(!checked){
						System.out.println("resernding package");
						checked = buffer.addWithCheck(addCheckBytes(s.getBytes()),check);
					}
					//System.out.println("Enter a message: ");
					//s = generate();
					switch(s){
					case "LETSCHANGE":
						changeFunction();
					break;
					}
				} else {
					System.out.println("buffer is full");
				}
				start = System.nanoTime();
				break;
			case "Server":
				try {
					Thread.sleep(400);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//if(new Random().nextInt(10)>5)
				if(buffer.bytes != null){
					if(buffer.bytes.peek() != null){
						result = new String(buffer.bytes.pop(), StandardCharsets.UTF_8);
						switch(result){
						case "HELLO":
							hello();
							break;
						case "LETSCHANGE":
							changeFunction();
							break;
						default:
							System.out.println(result);
							break;
						}
					} else {
						result = "buffer is empty";
					}
				}	
				break;
			}
		}
	}
	private String generate(){
		String gen;
		gen = "a";
		for(int i = 0;i<10;i++){
			gen = gen + random.nextInt(10);
		}
		return gen;
	}
	private byte[] addCheckBytes(byte[] b){
		byte[] c = new byte[b.length + 3];
		for(int i = 0;i<3;i++)
			c[i] = check[i];
		for(int i = 0;i<b.length;i++){
			c[check.length + i] = b[i];
		}
		return c;
	}
	private void hello(){
		System.out.println("hello from private function in " + this.toString());
	}
	private void changeFunction(){
		if(type == "Client"){
			buffer.clients--;
			buffer.servers++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(buffer.clients != buffer.servers){
				buffer.clients++;
				buffer.servers--;
				System.out.println("something went wrong");
				return;
			}
			type = "Server";
		} else if(type == "Server"){
			buffer.clients++;
			buffer.servers--;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(buffer.clients != buffer.servers){
				buffer.clients--;
				buffer.servers++;
				System.out.println("something went wrong");
				return;
			}
			type = "Client";
		}
		System.out.println("I'm (" + this.toString() + ") " + type + " now");
	}
}
