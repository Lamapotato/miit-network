package miit.study;

import java.util.concurrent.Semaphore;

public class MainClass {

	public static void main(String[] args) {
		System.out.println("started");
		Semaphore mutex1 = new Semaphore(0);
		Semaphore mutex2 = new Semaphore(1);
		Buffer buffer = new Buffer(5);
		Server client = new Server(buffer,mutex1,mutex2);
		Client server = new Client(buffer,mutex1,mutex2);
		Thread t0 = new Thread(server);
		Thread t1 = new Thread(client);
		t0.start();
		t1.start();
		//test
	}
}
