package miit.study;

import java.util.ArrayDeque;

public class Buffer {
	public int clients = 1;
	public int servers = 1;
	int size;
	ArrayDeque<byte[]> bytes = new ArrayDeque<byte[]>();
	Buffer(int size){
		this.size = size;
	}
	boolean addWithCheck(byte[] b,byte[] check){
		if(check[0] == 1 && check[1] == 1 && check[2] == 1){
			bytes.add(deleteCheckBytes(b));
			return true;
		}
		return false;
	}
	private byte[] deleteCheckBytes(byte[] b){
		byte[] c = new byte[b.length - 3];
		for(int i = 3;i<b.length;i++)
			c[i-3] = b[i];
		return c;
	}
}
